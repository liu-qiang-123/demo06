package com.hnevc.liuqiang;

public class Example01 {
    public static void main(String[] args) {

        int result = 0;
        try {
            result = divide(4, -2);
        } catch (DivideByMinusException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(result);

        System.out.println("我开始执行了");
    }

    //实现两个整数相除
    public static int divide(int x, int y) throws DivideByMinusException {
        if (y < 0) {
            throw new DivideByMinusException("除数为负数");
        }
        int result = x / y;
        return result;


    }
}

